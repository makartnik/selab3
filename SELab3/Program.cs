﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SELab3
{
    //Программа находит корни функиции f(x)=x*x*x-5*x*x-4*x+19
    //на интервале [-3;-1] методом Ньютона с задаваемым начальным приближением и с точностью 0.01
    /// <summary>
    /// Главный модуль приложения
    /// </summary>
    static class Program
    {
        /// <summary>
        /// Главная точка входа приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}
