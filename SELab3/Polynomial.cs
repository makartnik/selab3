﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SELab3
{
    /// <summary>
    /// Многочлен.
    /// </summary>
    class Polynomial : IFunction
    {
        public double[] CoefficientsDoubles { get; }

        /// <summary>
        /// Инициализирует поля класса
        /// </summary>
        /// <param name="coefficentsDoubles">Коэффиценты многочлена.</param>
        public Polynomial(params double[] coefficentsDoubles)
        {
            CoefficientsDoubles = coefficentsDoubles;
        }

        /// <summary>
        /// Возвращает частное значение функции.
        /// </summary>
        /// <param name="xDouble">Аргумент функции.</param>
        /// <returns>Частное значение функции в заданной точке.</returns>
        public double GetValue(double xDouble)
        {
            double sumDouble = 0;
            //Добавление к ответу значение каждого одночлена.
            for (int iInt = 0; iInt < CoefficientsDoubles.Length; ++iInt)
                sumDouble += CoefficientsDoubles[iInt] * Math.Pow(xDouble, iInt);
            return sumDouble;
        }

        /// <summary>
        /// Возвращает производную.
        /// </summary>
        /// <returns>Многочлен, являющийся производной.</returns>
        public IFunction GetDeriative()
        {
            double[] coefficentsDoubles = new double[CoefficientsDoubles.Length - 1];
            //Нахождение каоэффицентов производной.
            for (int iInt = 1; iInt < CoefficientsDoubles.Length; ++iInt)
                coefficentsDoubles[iInt - 1] = iInt * CoefficientsDoubles[iInt];
            return new Polynomial(coefficentsDoubles);
        }
    }
}
