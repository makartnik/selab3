﻿namespace SELab3
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.listView1ListView = new System.Windows.Forms.ListView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.numericUpDown1NumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.zedGraphControl1ZedGraphControl = new ZedGraph.ZedGraphControl();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1NumericUpDown)).BeginInit();
            this.SuspendLayout();
            // 
            // listView1
            // 
            this.listView1ListView.BackColor = System.Drawing.SystemColors.Control;
            this.listView1ListView.Dock = System.Windows.Forms.DockStyle.Right;
            this.listView1ListView.Location = new System.Drawing.Point(1019, 0);
            this.listView1ListView.Name = "listView1";
            this.listView1ListView.Size = new System.Drawing.Size(163, 627);
            this.listView1ListView.TabIndex = 0;
            this.listView1ListView.UseCompatibleStateImageBehavior = false;
            this.listView1ListView.View = System.Windows.Forms.View.List;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.numericUpDown1NumericUpDown);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(193, 627);
            this.panel1.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "X0:";
            // 
            // numericUpDown1
            // 
            this.numericUpDown1NumericUpDown.DecimalPlaces = 2;
            this.numericUpDown1NumericUpDown.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.numericUpDown1NumericUpDown.Location = new System.Drawing.Point(47, 12);
            this.numericUpDown1NumericUpDown.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.numericUpDown1NumericUpDown.Minimum = new decimal(new int[] {
            3,
            0,
            0,
            -2147483648});
            this.numericUpDown1NumericUpDown.Name = "numericUpDown1";
            this.numericUpDown1NumericUpDown.Size = new System.Drawing.Size(120, 22);
            this.numericUpDown1NumericUpDown.TabIndex = 0;
            this.numericUpDown1NumericUpDown.Value = new decimal(new int[] {
            2,
            0,
            0,
            -2147483648});
            this.numericUpDown1NumericUpDown.ValueChanged += new System.EventHandler(this.NumericUpDown1ValueChanged);
            // 
            // zedGraphControl1
            // 
            this.zedGraphControl1ZedGraphControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.zedGraphControl1ZedGraphControl.Location = new System.Drawing.Point(193, 0);
            this.zedGraphControl1ZedGraphControl.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.zedGraphControl1ZedGraphControl.Name = "zedGraphControl1";
            this.zedGraphControl1ZedGraphControl.ScrollGrace = 0D;
            this.zedGraphControl1ZedGraphControl.ScrollMaxX = 0D;
            this.zedGraphControl1ZedGraphControl.ScrollMaxY = 0D;
            this.zedGraphControl1ZedGraphControl.ScrollMaxY2 = 0D;
            this.zedGraphControl1ZedGraphControl.ScrollMinX = 0D;
            this.zedGraphControl1ZedGraphControl.ScrollMinY = 0D;
            this.zedGraphControl1ZedGraphControl.ScrollMinY2 = 0D;
            this.zedGraphControl1ZedGraphControl.Size = new System.Drawing.Size(826, 627);
            this.zedGraphControl1ZedGraphControl.TabIndex = 2;
            this.zedGraphControl1ZedGraphControl.UseExtendedPrintDialog = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1182, 627);
            this.Controls.Add(this.zedGraphControl1ZedGraphControl);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.listView1ListView);
            this.Name = "Form1";
            this.Text = "SELab3";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1NumericUpDown)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView listView1ListView;
        private System.Windows.Forms.Panel panel1;
        private ZedGraph.ZedGraphControl zedGraphControl1ZedGraphControl;
        private System.Windows.Forms.NumericUpDown numericUpDown1NumericUpDown;
        private System.Windows.Forms.Label label1;
    }
}

