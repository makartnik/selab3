﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SELab3
{
    /// <summary>
    /// Метод Ньютона.
    /// </summary>
    class Newton
    {
        private IFunction _functionIFunction;
        private IFunction _deriativeIFunction;

        /// <summary>
        /// Инициализирует поля класса.
        /// </summary>
        /// <param name="functionIFunction">Функция</param>
        public Newton(IFunction functionIFunction)
        {
            _functionIFunction = functionIFunction;
            _deriativeIFunction = functionIFunction.GetDeriative();
        }

        /// <summary>
        /// Возвращает значение аргумента следующей итерации.
        /// </summary>
        /// <param name="xDouble">Значение аргумента текущей итерации.</param>
        /// <param name="lowerBoundDouble">Нижняя граница интервала.</param>
        /// <param name="upperBoundDouble">Верхняя граница интервала.</param>
        /// <returns>Значение аргумента следующей итерации на заданном интервале.</returns>
        public double GetNextX(double xDouble, double lowerBoundDouble, double upperBoundDouble)
        {
            double nextXDouble = xDouble - _functionIFunction.GetValue(xDouble) / _deriativeIFunction.GetValue(xDouble);
            //Если значение следующей итерации меньше нижней границы интервала, 
            //вернуть середину отрезка между текущим значением и нижней границей
            if (nextXDouble < lowerBoundDouble)
                return (xDouble + lowerBoundDouble) / 2;
            //Если значение следующей итерации больше верхней границы интервала, 
            //вернуть середину отрезка между текущим значением и верхней границей
            if (nextXDouble > upperBoundDouble)
                return (xDouble + upperBoundDouble) / 2;
            return nextXDouble;
        }
    }
}
