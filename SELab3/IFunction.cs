﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SELab3
{
    /// <summary>
    /// Функция.
    /// </summary>
    interface IFunction
    {
        /// <summary>
        /// Возвращает частное значение функции.
        /// </summary>
        /// <param name="xDouble">Аргумент функции.</param>
        /// <returns>Частное значение функции в заданной точке.</returns>
        double GetValue(double xDouble);

        /// <summary>
        /// Возвращает производную.
        /// </summary>
        /// <returns>Функция, являющаяся производной.</returns>
        IFunction GetDeriative();
    }
}
