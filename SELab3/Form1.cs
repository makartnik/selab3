﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZedGraph;

namespace SELab3
{
    /// <summary>
    /// Форма приложения.
    /// </summary>
    public partial class Form1 : Form
    {
        private Newton _newtonNewton;
        private Polynomial _polynomialPolynomial;
        private const double _lowerBoundDouble = -3,
            _upperBoundDouble = -1,
            _epsDouble = 0.01;
        private LineItem _iterationsCurveLineItem;

        /// <summary>
        /// Инициализирует поля класса.
        /// </summary>
        public Form1()
        {
            InitializeComponent();
            _polynomialPolynomial = new Polynomial(19, -4, -5, 1);
            _newtonNewton = new Newton(_polynomialPolynomial);
            GraphPane paneGraphPane = zedGraphControl1ZedGraphControl.GraphPane;
            //Изменение заголовков
            paneGraphPane.Title.IsVisible = false;
            paneGraphPane.XAxis.Title.Text = "X";
            paneGraphPane.YAxis.Title.Text = "F(X)";
            //Рисование начального графика
            LineItem polynomialCurveLineItem = GetFunctionCurve(_polynomialPolynomial, _lowerBoundDouble, _upperBoundDouble, 0.001, Color.Red);
            paneGraphPane.CurveList.Add(polynomialCurveLineItem);
            _iterationsCurveLineItem = paneGraphPane.AddCurve("",new PointPairList(),Color.Blue,SymbolType.Circle);
            _iterationsCurveLineItem.Symbol.Fill.Type = FillType.Solid;
            _iterationsCurveLineItem.Line.IsVisible = false;
            _iterationsCurveLineItem.Symbol.Size = 5;
            UpdatePoints();
        }

        /// <summary>
        /// Обновляет линию с итерациями метода Ньютона.
        /// </summary>
        public void UpdatePoints()
        {
            PointPairList pointsPointsPairList = GetNewtonPoints((double)numericUpDown1NumericUpDown.Value);
            _iterationsCurveLineItem.Points = pointsPointsPairList;
            listView1ListView.Clear();
            //Добавление каждой точки в ListView.
            foreach (PointPair pointPointPair in pointsPointsPairList)
                listView1ListView.Items.Add(string.Format("x={0:F4} f(x)={1:F4}", pointPointPair.X, pointPointPair.Y));
            zedGraphControl1ZedGraphControl.AxisChange();
            zedGraphControl1ZedGraphControl.Invalidate();
        }


        /// <summary>
        /// Возвращает коллекцию с итерациями метода Ньютона
        /// </summary>
        /// <param name="x0Double">Начальное положение</param>
        /// <returns>Список точек с итерациями</returns>
        private PointPairList GetNewtonPoints(double x0Double)
        {
            PointPairList pointsPointPairList = new PointPairList();
            double xDouble = x0Double;
            double fxDouble = _polynomialPolynomial.GetValue(xDouble);
            pointsPointPairList.Add(xDouble, fxDouble);
            //Выполнять пока абсолютное значение функции больше погрешности.
            while (Math.Abs(fxDouble) >= _epsDouble)
            {
                xDouble = _newtonNewton.GetNextX(xDouble, _lowerBoundDouble, _upperBoundDouble);
                fxDouble = _polynomialPolynomial.GetValue(xDouble);
                pointsPointPairList.Add(xDouble, fxDouble);
            }
            return pointsPointPairList;
        }

        /// <summary>
        /// Выполняется при изменении значения numericUpDown1NumericUpDown.
        /// </summary>
        /// <param name="sender">Объект, вызвавший событие.</param>
        /// <param name="e">Параметры события.</param>
        private void NumericUpDown1ValueChanged(object sender, EventArgs e)
        {
            UpdatePoints();
        }

        /// <summary>
        /// Возвращает линию графика функции.
        /// </summary>
        /// <param name="functionIFunction">Функция</param>
        /// <param name="lowerBoundDouble">Нижняя граница графика.</param>
        /// <param name="upperBoundDouble">Верхняя граница графика.</param>
        /// <param name="stepDouble">Интервал между соседними точками на графике.</param>
        /// <param name="colorColor">Цвет линии.</param>
        /// <returns>Линия графика.</returns>
        private LineItem GetFunctionCurve(IFunction functionIFunction, double lowerBoundDouble, double upperBoundDouble, 
            double stepDouble, Color colorColor)
        {
            PointPairList pointsPointsPairList = new PointPairList();
            //Добавление каждой точки в список на интервале
            for (double xDouble = lowerBoundDouble; xDouble <= upperBoundDouble; xDouble += stepDouble)
                pointsPointsPairList.Add(xDouble, functionIFunction.GetValue(xDouble));
            return new LineItem("", pointsPointsPairList, colorColor, SymbolType.None);
        }
    }
}
